export default {
  mode: 'spa',
  head: {
    title: 'Code the Shrinking World',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' }
    ]
  },
  loading: { color: '#000' }
}
